# Master 2 bioinformatique - parcours BioInformatique en Santé (BIS)

L'année de M1 est principalement consacrée à acquerir un socle équilibré de compétences fortes en informatique (~200h en M1) et en génomique-génétique (178h en M1). 
En informatique, l'accent est mis sur l'analyse et gestion de données massives, les systèmes informatiques et la programmation. 
Un socle de compétences en biologie et plus particulièrement en génomique et analyses de données issues des nouvelles technologies de séquençage sont également développées.

Dans le parcours **Bioinformatique en santé**, la deuxième année de master ne comporte plus d'enseignements d'informatique, mais permet de mettre en pratique et en perspectives les compétences acquises au cours de la première année. 

Les cours ont lieu de septembre à décembre, et sont suivis par un stage de 6 mois.
Les stages à l'étranger sont encouragés !

## Compétences

![UE Master2 bioinformatique Rennes parcours BIS](img/schemaM2BIS-UE-2018.png)

![Récapitulatif Master2 bioinformatique Rennes parcours BIS](img/schemaM2BIS-recap-2018.png)

## Débouchés

Le parcours BIS débouche sur des postes d'ingénieur.e d'étude dans des équipes de recherche en génomique dans le domaine de la santé (notamment INSERM et INRA) ou au sein de structures hospitalières, ainsi que par une poursuite d'étude en doctorat.

## Stages types


