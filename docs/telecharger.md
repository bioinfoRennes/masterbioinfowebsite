# Télécharger

- [Plaquette de présentation générale du master](master_bioinformatique_fiche2019.pdf)
- [Présentation du master](master_bioinformatique.pdf) (présentation générale)
- [Présentation du master](master_bioinformatique_sante.pdf) (pour les étudiants en cursus santé)
- [Description des UE](BrochureBioinformatique2018.pdf)

## Ressources de la [Société Française de Bioinformatique (SFBI)](https://www.sfbi.fr)

- [La bioinformatique : métier, recrutement, compétences](https://www.sfbi.fr/metiers-formations)
- [Fiche métier de bioinformaticien.ne](https://www.sfbi.fr/fiche_metier_bioinfo)

## Historique

- [L'histoire du master de bioinformatique de Rennes depuis sa création en 2000 (Christian Delamarche et Dominique Lavenier, 2021-11-26)](master_bioinformatique_Rennes_historique-20211126.pdf)
