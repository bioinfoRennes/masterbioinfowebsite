# Master bioinformatique Rennes 1

Le master de bioinformatique de l'[Université de Rennes1](https://www.univ-rennes1.fr) forme des **expert.e.s en sciences de données biologiques et génomiques** à l'interface de la santé, de l'environnement, des données massives et des technologies de l'information.
La formation délivre un **socle de compétences communes en génomique et en informatique**.
Dans ces deux domaines, la formation prend le temps d'introduire les notions utiles en fonction du cursus d'origine des étudiant.e.s.
De nombreux [stages et projets](stages.md) permettent d'approfondir ces notions.
Nous encourageons les stages à l'étranger et la participation à des compétitions internationales.

À l'issue du master, ces compétences permettent aux étudiant.e.s d'occuper des postes d'ingénieur.e d'étude au sein d'une plateforme de bioinformatique ou d'une équipe de recherche en génomique, de chef.fe de projet dans le secteur industriel (agronomie, industrie pharmaceutique) ou encore de poursuivre par un doctorat.
La bioinformatique est un secteur en plein essor et les offres d'emploi y sont nombreuses (nous vous invitons à consulter l'[analyse des offres d'emploi de la SFBI](https://bioinfo-fr.net/etat-de-lemploi-bioinformatique-en-france-analyse-des-offres-de-la-sfbi)).
Ces carrières concernent aussi bien la France que l'international (Europe, Amérique du Nord, Australie).

Le master de bioinformatique est porté par l'UFR [SVE](https://sve.univ-rennes1.fr/).
Il bénéficie d'un [accord Erasmus+ avec l'université de Salerne](http://www.unisa.it) (Italie), ainsi que d'une convention avec les filières [Statistiques pour les sciences de la vie](http://www.ensai.fr/academics/third-year/biostatistics-specialization.html) et [Statistiques et ingénierie des données](http://www.ensai.fr/academics/third-year/data-science-specialization.html) de l'[ENSAI](http://www.ensai.fr) (les élèves ingénieurs de troisième année sélectionné.e.s peuvent suivre respectivement les parcours [BIS](bis.md) et [IBI](ibi.md)).

**À partir de la rentrée, le master passe à une nouvelle maquette, dans laquelle on retrouvera les trois grands thèmes du master. Le nouveau site Web sera en ligne dans les jours prochains...** *stay tuned!*


## Compétences

* Le master 1 est un tronc commun qui fournit un socle de compétences en génomique, nouvelles technologies de séquençage (NGS), en génétique et en informatique avec une attention particulière pour les données massives.

* Le master 2 comporte trois parcours :
    * **[Analyse de données génomiques (ADG)](adg.md)** se spécialise sur l'utilisation de méthodes de pointe pour l'analyse de données *omiques, l'exploitation des bases de données de référence et l'utilisation d'infrastructures de calcul intensif. C'est la partie de la bioinformatique la plus proche de la biologie ;
    * **[Bioinformatique en santé (BIS)](bis.md)** se spécialise sur les enjeux actuels de la génétique et de la génomique en santé : aide au diagnostic, génomique et cancérologie, génétique de l’hérédité simple et complexe, ingénierie génétique, ainsi que sur la conception de thérapeutiques nouvelles et personnalisées. C'est la partie de la bioinformatique centrée sur les données biomédicales ;
    * **[Informatique et biologie intégrative (IBI)](ibi.md)** se spécialise sur le développement de méthodes novatrices d'analyse de données biologiques grâce à des compétences en algorithmique et en programmation. C'est la partie de la bioinformatique la plus proche de l'informatique.

![Compétences master bioinformatique Rennes](img/schemaGeneral-2018.png)

## Recrutement et candidater

Le master de bioinformatique est ouvert aux étudiant.e.s biologistes, informaticien.ne.s, mathématicien.ne.s, bioinformaticien.ne.s (évidemment) ayant obtenu une licence ou un DUT, ainsi qu'aux étudiant.e.s en cursus santé, en parallèle avec leurs études de médecine. 
Nous accueillons également des étudiant.e.s au profil atypique ou en reprise d'étude via la formation continue.

Candidature possible au niveau master 2.

Plus d'infos aux pages [Recrutement selon votre cursus](recrutement.md) et [candidater](candidater.md).
Le master a également la chance de bénéficier du dynamisme de l'[association des étudiant.e.s e-BIGO](https://ebigo-rennes.jimdo.com/) ; n'hésitez pas à les contacter pour vous renseigner, elle pourra vous mettre en relation avec des étudiant.e.s (actuel.le.s ou ancien.ne.s) ayant un profil similaire au vôtre.

![Master Bioinformatique Rennes 2022-2023](img/Trombi_M1M2_x250.jpg)



## Actualités

![BIG Day 2021 Master Bioinformatique Rennes](img/BIGday2022.jpeg)

* **Prochaine maquette 2023-2024 et nouveau site web** : à partir de la rentrée, le master passe à une nouvelle maquette, dans laquelle on retrouvera les trois grands thèmes du master. Le [site Web du nouveau master est en ligne](http://bioinfo-rennes.fr/newSite/)
* [**Ouest France fiche métier : Bio-informaticienne, Stéphanie Robin décrypte le vivant grâce au numérique**](https://www.ouest-france.fr/education/orientation/fiches-metiers/video-bio-informaticienne-stephanie-robin-decrypte-le-vivant-grace-au-numerique-511cec37-1cba-455f-9ca2-1bf77693aca6)
* [**Ouest France : Formation. Quatre questions sur le métier de bio-informaticien (2023-02-04)**](https://www.ouest-france.fr/education/orientation/fiches-metiers/formation-quatre-questions-sur-le-metier-de-bio-informaticien-10ea1620-8d07-11ed-8162-cc23f8f624e9)
* **BIG Day 2022 : vendredi 18 novembre 2022** : Conférences autour de la bioinformatique, parcours d'ancien.ne.s étudiant.e.s et remise des diplômes. Le thème de cette année sera **les domaines atypiques de la bioinformatique**, et le parrain sera [Dominique Lavenier](http://lavenier.net/homepage/). ([Programme sur le site de l'association des étudiant.e.s en bioinformatique e-BIGO](https://ebigo-rennes.jimdofree.com/big-day/)). La journée sera [retransmise sur youtube](https://www.youtube.com/@e-bigo)
* **Rentrée M1 et M2 lundi 05 septembre 2022 14h amphi PNRB Beaulieu** [Pôle Numérique Rennes-Beaulieu](https://umap.openstreetmap.fr/fr/map/acces-au-pnrb_222022#17/48.11391/-1.64065). Il faudra sans doute emprunter l'entrée Sud, côté avenue du Gal Leclerc, pas l'entrée Nord côté Pôle Images et Réseaux.
* **Candidatures 2022-2023 :** La campagne de candidature sera ouverte **du 26 avril au 06 juin 2022**. Plus d'infos sur [notre page candidater](candidater.md)
* **Portes ouvertes virtuelles : mercredi 02 mars 2022** : Une [visioconférence avec l'équipe pédagogique](https://univ-rennes1-fr.zoom.us/j/5629006947) comportera une présentation du master et nous permettra de répondre à vos questions (code = 56-29-86 en enlevant les tirets)
* **BIG Day 2021 : vendredi 26 novembre 2021** : Conférences autour de la bioinformatique, parcours d'ancien.ne.s étudiant.e.s, les 20 ans du master de bioinfo de Rennes et remise des diplômes. Le thème de cette année sera le **machine learning**, et le parrain sera [Laurent Jacob](https://lbbe.univ-lyon1.fr/fr/annuaire-des-membres/jacob-laurent). ([Programme sur le site de l'association des étudiant.e.s en bioinformatique e-BIGO](https://ebigo-rennes.jimdofree.com/big-day/edition-2021/)). La journée sera [retransmise sur youtube](https://www.youtube.com/channel/UCEfhXxLHGn_zogYKKuEdxww)
* **Rentrée M1 et M2 mercredi 01 septembre 2021 14h amphi PNRB Beaulieu** [Pôle Numérique Rennes-Beaulieu](https://umap.openstreetmap.fr/fr/map/acces-au-pnrb_222022#17/48.11391/-1.64065). Il faudra sans doute emprunter l'entrée Sud, côté avenue du Gal Leclerc, pas l'entrée Nord côté Pôle Images et Réseaux.
* **Candidatures 2021-2022 :** La campagne de candidature sera ouverte **du 26 avril au (~~17~~ 19 mai 2021 via médecine pour M2 BIS suite aux problèmes rencontrés par le portail de candidatures) ou (06 juin 2021 via SVE pour M1 et M2 ADG et IBI)**. Plus d'infos sur [notre page candidater](candidater.md)
* **BIG Day 2020 : vendredi 27 novembre 2020** : Conférences autour de la bioinformatique, parcours d'ancien.ne.s étudiant.e.s et remise des diplômes. Le thème de cette année sera la **biologie des systèmes et computationnelle**, et **la marraine sera [Patricia Thébault](https://dept-info.labri.fr/~thebault/)** du [LaBRI](https://www.labri.fr/) à Bordeaux. ([Programme sur le site de l'association des étudiant.e.s en bioinformatique e-BIGO](https://ebigo-rennes.jimdo.com/)).
* **Rentrée M1 et M2 mercredi 02 septembre 2020 :** à 09h au [Pôle Numérique Rennes-Beaulieu](https://umap.openstreetmap.fr/fr/map/acces-au-pnrb_222022#17/48.11391/-1.64065)
* **Compte-rendu BIG day 2019 :** L'association [eBIGO](http://bioinfo-rennes.fr/) a rédigé un compte-rendu détaillé du [BIGday et de la remise des diplômes 2019](https://ebigo-rennes.jimdofree.com/2020/01/06/big-day-2019-synth%C3%A8se-d-une-belle-%C3%A9dition/)
* **Journée portes ouvertes 08 février 2020 :** Les étudiant.e.s et l'équipe pédagogique seront présent.e.s pour représenter le master sur le [campus sciences](https://www.openstreetmap.org/?mlat=48.11541&mlon=-1.63694#map=18/48.11541/-1.63694) (Beaulieu) et le [campus santé](https://www.openstreetmap.org/?mlat=48.11785&mlon=-1.69586#map=18/48.11785/-1.69586) (Villejean) lors de la [Journée portes ouvertes du samedi 08 février 2020](https://www.univ-rennes1.fr/evenements/22102019/portes-ouvertes-2020).
* **Fiche métier Bioinformaticien.ne :** La [Société Française de Bioinformatique (SFBI)](https://www.sfbi.fr) a réalisé une [fiche générique du métier de bioinformaticien.ne](https://www.sfbi.fr/fiche_metier_bioinfo)
* **BIG Day 2019 : 29 novembre 2019 au Diapason :** Conférences autour e la bioinformatique, parcours d'ancien.ne.s étudiant.e.s et remise des diplômes ([Programme sur le site de l'association des étudiant.e.s en bioinformatique e-BIGO](https://ebigo-rennes.jimdo.com/)). La marraine de la promotion 2019 est [Anna-Sophie Fiston-Lavier](https://annasfistonlavier.com/) de l'équipe [Phylogenie et evolution moleculaire](http://www.isem.univ-montp2.fr/fr/equipes/phylogenie-et-evolution-moleculaire-index/) à l Institut des Sciences de l’Evolution de Montpellier (ISE-M).
* **Lucas Bourneuf lauréat de Angry birds AI competition!**: [Lucas Bourneuf](https://lucas.bourneuf.net/home/) est un ancien du master qui a [gagné la compétition Man vs Machine de la (prestigieuse) Angry Birds AI Competition](https://www.aibirds.org/man-vs-machine-challenge/previous-results.html). Félicitations!
* **Qu'est-ce que la bioinformatique ?** : la [Société Française de Bioinformatique (SFBI](https://www.sfbi.fr) propose une explication sur [la bioinformatique, les métiers et les formations](https://www.sfbi.fr/node/11348)
* **Ouest-France: "Un Master en sciences, une voie royale vers l’emploi"** : l'article "[Un Master en sciences, une voie royale vers l’emploi](https://www.ouest-france.fr/bretagne/rennes-35000/un-master-en-sciences-une-voie-royale-vers-l-emploi-6280420)" montre que l'insertion professionelle des masters de sciences de l'Université de Rennes 1 est bonne.  À comparer avec le [devenir professionnel de nos étudiants](statistiquesEmploi.md) qui montre qu'ils/elles s'en sortent bien aussi :-) 
* **Journée portes ouvertes samedi 02 février 2019** : dans le cadre des portes ouvertes de l'Université de Rennes 1, le master bioinformatique sera représenté par des enseignant.e.s et des étudiant.e.s le samedi 02 février **sur les campus de Beaulieu et de Villejean**
* **La bioinformatique en France : étude de la SFBI :** Les [rencontres 2018 autour de l'enseignement de la bioinformatique en France organisées par la SFBI](https://www.sfbi.fr/node/11298) ont permi de dresser un [état des lieux de la bioinformatique](https://www.sfbi.fr/sites/sfbi.fr/files/pictures/2017-10/fascicule_2018.pdf)
* **Statistiques d'emploi :** mise à jour avec les [statistiques d'emploi de la promotion 2018](statistiquesEmploi/)
* **BIG Day et remise des diplômes : 23 novembre 2018 au Diapason** Plus d'infos [sur le site de l'association e-BIGO des étudiants en BioInformatique du Grand Ouest](https://ebigo-rennes.jimdo.com/2018/11/13/programmation-big-day-2018/)
* **Carrières hospitalo-universitaires : [l'IGAS encourage les doubles cursus médecine/science](https://www.lequotidiendumedecin.fr/actualites/article/2018/10/23/carrieres-hospitalo-universitaires-post-internat-acces-au-secteur-ii-ligas-veut-changer-les-regles_862058)** Le master de bioinformatique de Rennes propose déjà ce dispositif et est parfaitement adapté.
* **[Sciences en cour[t]s](http://sciences-en-courts.fr/) : festival de très courts métrages de vulgarisation scientifique le 18 octobre 2018 au Diapason** La bioinfo y est traditionellement bien représentée, avec notamment une très forte implication d'ancien.ne.s du master
* **Dernier amphi de Christan Delamarche** : le 28 septembre 2018, c'était le dernier cours de Christian Delamarche. Les master 1 (bon, ils avaient justement cours avec lui), les master 2, d'ancien.ne.s étudiant.e.s et les collègues sont venus à la fin du cours fêter ce moment fort. 
* **Erasmus+ : visite du Pr Anna Marabotti (univ. Salerno) en 2019** Nous aurons la chance d'avoir à nouveau la visite d'[Anna Marabotti (univ. Salerno)](http://docenti.unisa.it/025916/home) en janvier.
* **La bioinformatique en France en 2018** : L'excellent blog [bioinfo-fr.net](https://bioinfo-fr.net/) a réalisé une enquête et vient d'en [publier les résultats](https://bioinfo-fr.net/enquete-bioinfo-fr-2018-portrait-de-bioinfo). À comparer avec les [statistiques d'emploi](statistiquesEmploi.md) de notre promo 2017
* La [Brochure du master](BrochureBioinformatique2018.pdf) décrivant les différentes UE ainsi que la [présentation du master](master_bioinformatique.pdf) sont en ligne !
* Participation d'étudiants du master au **[Challenge Digital Transformer](https://digitaltransformer.univ-rennes1.fr/)**
* **Erasmus+** : Dans le cadre de l'accord [Erasmus+](http://ec.europa.eu/programmes/erasmus-plus/node_fr) entre le master de bioinformatique de Rennes et l'[Université de Salerne en Italie](http://web.unisa.it/en), **[Anna Marabotti (univ. Salerno)](http://docenti.unisa.it/025916/home)** est venue faire cinq cours de 2h sur la bioinformatique structurelle, et une présentation des mobilités Erasmus+
* **[Statistiques d'emploi](statistiquesEmploi.md)** de la promo 2017
* **BIG day 17 nov. 2017** : La cérémonie de remise des diplômes aux étudiant.e.s de la promotion 2017 s'est déroulée au Diapason en présence de leur marraine [Sylvie Ricard-Blum (ICBMS, Univ. Lyon)](http://icbms.fr/annuaire/198-ricard-blum)
* **Applications Android** : Dans le cadre des projets de 1ère année, des étudiant.e.s ont réalisé [ARPoster](https://play.google.com/store/apps/details?id=com.rennes1.BIG) (**réalité augmentée**) et [3.2.1.Bio!](https://play.google.com/store/apps/details?id=com.a321bio.a321bioprj) (un **jeu de quizz sur la biologie**)
* [Marine Brenet (une ancienne du master) en charge de EnDirectDuLabo du 25 au 29 septembre 2017](http://endirectdulabo.tumblr.com/). EnDirectDuLabo est un site collaboratif associé à un compte tweeter [@EnDirectDuLabo]. Chaque semaine, une personne différente y présente son parcours, son travail et ses projets.
* [Le BIG day parmi les journées organisées par les masters de bioinformatique](https://etudes.univ-rennes1.fr/master-biogeno/themes/Annonces/Le_BIG_day_parmi_les_journ_es_organis_es_par_les_masters_de_bioinformatique.cid18154)
* [Cartographie des formations en bioinformatique](https://etudes.univ-rennes1.fr/master-biogeno/themes/Annonces/Cartographie_des_formations_en_bioinformatique.cid18091)
* [Enquête emplois en bioinformatique](https://etudes.univ-rennes1.fr/master-biogeno/themes/Annonces/Enqu_te_emplois_en_bioinformatique.cid18009)
* [Des master2 au dev jam](https://etudes.univ-rennes1.fr/master-biogeno/themes/Annonces/Des_master2_au_dev_jam.cid17902)
* [Remise des diplômes 2015](https://etudes.univ-rennes1.fr/master-biogeno/themes/Annonces/Remise_des_dipl_mes_2015___on_parle__encore__du_master_sur_bioinfo-fr.net.cid17901) : on parle (encore) du master sur [bioinfo-fr.net](https://bioinfo-fr.net/remise-des-diplomes-du-master-big-rennes)
* [On parle du master](https://etudes.univ-rennes1.fr/master-biogeno/themes/Annonces/On_parle_du_master_sur_le_blog_bioinfo-fr.net__.cid17610) sur le blog [bioinfo-fr.net](https://bioinfo-fr.net/presentation-du-master-bioinformatique-genomique-de-rennes) !
* [Participation Rennaise à la compétition iGEM2014 de biologie synthétique](https://etudes.univ-rennes1.fr/master-biogeno/themes/Annonces/Participation_Rennaise___la_comp_tition_iGEM2014_de_biologie_synth_tique.cid17475)

## Liens
* [e-BIGO : Association des étudiant.e.s du master](https://ebigo-rennes.jimdo.com/)
* [La Société Française de Bioinformatique (SFBI)](https://www.sfbi.fr)
* [L'ancien site du master BIG](https://etudes.univ-rennes1.fr/master-biogeno)
* [JeBiF : l'association des jeunes bioinformaticiens de France](https://jebif.fr)
* [Le dépôt gitlab du master](https://gitlab.com/bioinfoRennes)

## Contact

[contactez les responsables du master](mailto:master-bioinfo@univ-rennes1.fr)


