# Master 2 bioinformatique - parcours Analyse de Données Génomiques (ADG)

L'année de M1 est principalement consacrée à acquerir un socle équilibré de compétences fortes en informatique (~200h en M1) et en génomique-génétique (178h en M1). 
En informatique, l'accent est mis sur l'analyse et gestion de données massives, les systèmes informatiques et la programmation. 
Un socle de compétences en biologie et plus particulièrement en génomique et analyses de données issues des nouvelles technologies de séquençage sont également développées.

Dans le parcours **Analyse de Données Génomiques**, la deuxième année de master ne comporte plus d'enseignements d'informatique, mais permet de mettre en pratique et en perspectives les compétences acquises au cours de la première année. 

Les cours ont lieu de septembre à décembre, et sont suivis par un stage de 6 mois.
Les stages à l'étranger sont encouragés !


## Compétences

Les étudiant.e.s du parcours Analyse de données génomiques auront acquis une formation solide sur 2 ans leur permettant d’analyser les données de génomique, protéomique, transcriptomique, épigénétique etc, massivement produites par les laboratoires de recherche, plateformes et structures privées. 
Ils seront suffisamment à l’aise avec l’informatique pour être autonomes dans leurs calculs et utiliser des infrastructures de calcul intensif (type cluster partagé). Les étudiant.e.s auront balayé un large éventail de domaines d’application, de la génomique.


![UE Master2 bioinformatique Rennes parcours ADG](img/schemaM2ADG-UE-2018.png)

![Récapitulatif Master2 bioinformatique Rennes parcours ADG](img/schemaM2ADG-recap-2018.png)

## Débouchés

Le parcours ADG débouche sur des postes d'ingénieur.e d'étude dans des équipes de recherche en génomique ou au sein de plateformes de bioinformatique ou dans un groupe industriel en agronomie ou pharmaceutique, ainsi que par une poursuite d'étude en doctorat.

## Stages types


