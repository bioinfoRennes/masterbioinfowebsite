# Candidater au master de bioinformatique de l'Université de Rennes 1


## Recrutement

### En master 1

Le master de bioinformatique est ouvert aux étudiant.e.s biologistes, informaticien.ne.s, mathématicien.ne.s, bioinformaticien.ne.s (évidemment) ayant obtenu une licence ou un DUT, ainsi qu'aux étudiant.e.s en cursus santé, en parallèle avec leurs études de médecine. 

Nous accueillons également des étudiant.e.s au profil atypique ou en reprise d'étude via la formation continue.


### En master 2

Il est possible d'intégrer la formation en master 2 après examen de votre dossier par la commission de recrutement.
Pour cela, il faut évidemment avoir validé un master 1 ou équivalent.
En fonction du cursus antérieur, nous pouvons être amené.e.s à proposer un aménagement des enseignements sous forme de contrat pédagogique.

Cette possibilité s'adresse en particulier (mais pas uniquement) aux :

* étudiant.e.s du **cursus santé** internes et ayant déjà validé un master 1 éventuellement ailleurs qu'à Rennes
* étudiant.e.s des filières [Statistiques pour les sciences de la vie](http://www.ensai.fr/academics/third-year/biostatistics-specialization.html) et [Statistiques et ingénierie des données](http://www.ensai.fr/academics/third-year/data-science-specialization.html) de l'[ENSAI](http://www.ensai.fr)
* personnes en reprise d'étude via la formation continue.

## Candidater

**La procédure de candidature est ouverte du 26 avril au 06 juin 2022**.
Elle se fait exclusivement via les portails de l'université de Rennes 1 :

- [candidater en formation initiale](https://candidatures.univ-rennes1.fr)
- [candidater en formation continue](https://candidatures-sfca.univ-rennes1.fr/)

Vous pouvez ensuite **déposer des pièces complémentaires jusqu'au 15 juin 2022** (relevés de notes et classement du second semestre,...). **N'hésitez pas à nous communiquer toute information qui pourrait venir conforter votre candidature**.

Les **étudiant.e.s internationaux** peuvent [candidater pour des bourses auprès de la Fondation Rennes1](https://fondation.univ-rennes1.fr/accueil-des-etudiants-internationaux-en-master-oriente-recherche). Prochain appel : **mars 2022**.


![Où cliquer pour s'inscrire au master](img/candidater01offreFormationAnnotee.png)


Pour nous aider à mieux évaluer votre candidature, nous vous suggérons de joindre à votre dossier tous les éléments que vous jugerez utiles, notamment :

* un **CV**
* une **lettre de motivation** exposant le(s) projet(s) professionnel(s) et argumentant sur la valeur ajoutée du master bioinformatique par rapport à ce(s) projet(s) ;
* vos **relevés de notes** ;
* des **justificatifs de votre niveau de programmation et d'utilisation des systèmes unix ou GNU/Linux** (notes, lien vers votre dépôt git, participation à des moocs, rapport de projet...) ;
* (optionnel) une indication de votre niveau de Français (et/ou Anglais) validé ;
* (pour les candidats en master2), une auto-évaluation sur les connaissances fondamentales considérées comme pré-requises, avec des éléments de justifications.

**Vous avez des questions ?** n'hésitez pas à [prendre contact avec l'équipe pédagogique](mailto:master-bioinfo@univ-rennes1.fr).
l'[association des étudiant.e.s e-BIGO](https://ebigo-rennes.jimdo.com/) ; elle pourra vous mettre en relation avec des étudiant.e.s (actuel.le.s ou ancien.ne.s) ayant un profil similaire au vôtre et pourront vous donner leur impression sur le master.

