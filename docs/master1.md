# Master 1 bioinformatique

Le master 1 fournit un socle de compétences communes en génomique, NGS, génétique, programmation et gestion des données massives.
Ces compétences permettent aux étudiants d'être autonomes dans la réalisation d'analyses bioinformatique ainsi que dans l'organisation et l'administration des solutions techniques.


## Organisation

Le master 1 est commun à tous les étudiant.e.s.

* Il commence par une session de mise à niveau début septembre, avec un approfondissement en génomique, informatique et/ou statistiques

* Au cours de l'année, les différentes UE concernent les trois groupes principaux de compétences :
    * **génétique et génomique** (180h)
    * **informatique** (200h)
    * compétences transversales et **insertion professionnelle** (120h)

* À partir d'avril, les étudiant.e.s peuvent s'engager dans un **stage (2 à 3 mois, prolongeable)**, participer à une **compétition internationale** ou s'engager dans un **projet personnel** (startup,...)


## UE

La liste complète des UE et leurs descriptions sont détaillées dans [notre brochure](./BrochureBioinformatique2018.pdf) et sur le [site de l'offre de formation de l'université de Rennes 1](https://sve.univ-rennes1.fr/formations/master-1-bioinformatique).

![UE Master1 bioinformatique Rennes](img/schemaM1-2018.png)

## Après le master 1

Les étudiant.e.s ayant validé le master 1 sont d'office admis en [master 2](master2.md) où ils doivent choisir l'un des trois parcours :

* **[Analyse de données génomiques](adg.md)** (ADG)

* **[Bioinformatique en santé](bis.md)** (BIS)

* **[Informatique et biologie intégrative](ibi.md)** (IBI)

