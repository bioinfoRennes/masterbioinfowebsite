# Stages, projets et insertion professionelle

![Stages master bioinformatique Rennes](img/masterBioinfoRennes_cumul_stages_superposition.png)


Les **stages** et les **projets** occupent une place importante dans notre formation car c'est un excellent moyen :

- de mettre en oeuvre les **compétences** acquises au cours du master sur des cas concrets et réalistes, qui vont au delà des exemples idéaux que l'on peut aborder en quelques séances de TP ;
- d'approfondir les notions vues en cours et qui vous plaisent particulièrement (voir d'en **découvrir de nouvelles**) ;
- d'acquérir une foule de **savoirs techniques** et de **savoirs être** ;
- de travailler à votre **insertion professionelle** et d'élargir votre réseau.

Ces différents aspects sont intégrés tout au long de la formation et à chaque semestre, plutôt que d'être abordés de façon ponctuelle :

- Au premier semestre (M1) : 
    - UE "Expression et communication"
    - Participation à deux tables rondes
    - Participation au "BIG day" (remise des diplômes de la promotion sortante, séminaires et conférence d'un(e) invité(e) ayant joué un rôle majeur dans le domaine de la bioinformatique)
- Au second semestre (M1) :
    - UE "Mise en pratique : aspect technique, cadre juridique et économique" : il s'agit de cours sur les notions de base en économie et en propriété intellectuelle, plus au choix trois mois pour :
        - travailler sur un projet en lien avec votre projet professionnel (monter une startup,...) ;
        - participer à une compétition internationale ;
        - faire un stage de 3 mois dans un laboratoire ou une entreprise.
- Au troisième semestre (M2) :
    - Organisation et participation au "BIG day"
    - UE "Connaissance du tissu actuel et tendances émergentes" : conférences et séminaires sur des sujets pointus en bioinformatique
- Au quatrième semestre (M2) : 
    - vous êtes en stage long (6 mois) dans un laboratoire ou une entreprise, en France ou à l'étranger.


## Stages

Le master reçoit de nombreuses propositions de stages de la part de laboratoires et d'entreprises qui ont déjà accueilli nos étudiant.e.s par le passé.
De plus, nous encourageons les étudiant.e.s à rechercher de manière autonome les terrains de stage qui correspondent le mieux à leurs aspirations (et nous les aidons à le faire).

- Stage de M1 : optionnel, d'avril à juin (possibilité de prolonger durant l'été)
- Stage de M2 : obligatoire, de janvier à juin


## Projets / compétitions internationales

Nous encourageons les étudiant.e.s à participer à des compétitions internationales comme [igem](http://igem.org/), [kaggle](https://www.kaggle.com/competitions), etc. ou au [Google summer of code](https://developers.google.com/open-source/gsoc/).
