# Recrutement selon votre cursus antérieur

Le master de bioinformatique est ouvert aux étudiant.e.s biologistes, informaticien.ne.s, mathématicien.ne.s, bioinformaticien.ne.s (évidemment) ayant obtenu une licence ou un DUT, ainsi qu'aux étudiant.e.s en cursus santé, en parallèle avec leurs études de médecine. Nous accueillons également des étudiant.e.s au profil atypique ou en reprise d'étude via la formation continue. N'hésitez pas à contacter également l'[association des étudiant.e.s e-BIGO](https://ebigo-rennes.jimdo.com/) ; elle pourra vous mettre en relation avec des étudiant.e.s (actuel.le.s ou ancien.ne.s) ayant un profil similaire au vôtre.

## Vous êtes titulaire d’une licence de biologie, d’informatique, de bioinformatique, de statistiques ou de mathématiques
Vous pouvez candidater pour entrer en [master 1](master1.md) et vous suivrez la version standard.

## Vous êtes étudiant(e) en cursus santé
Vous pouvez candidater pour effectuer votre **master 1 en parallèle à vos études de santé**. 
Vous avez le choix entre deux possibilités :

### Cursus santé version standard
Les étudiant.e.s de médecine, pharmacie et odontologie valident leur master 1 à la fin de leur quatrième année après avoir validé les UE suivantes au cours de leur deuxième, troisième ou quatrième année :

- Programmation impérative en Python (6 ECTS)
- Bioinformatique et génomique 1&2 (6 ECTS)
- Advanced statistics with R (3 ECTS)
- Next Generation Sequencing (3 ECTS)
- Bases de données relationnelles ( 6 ECTS)
- Stage (6 ECTS)

### Cursus santé version double cursus
Les étudiant.e.s de médecine sélectionnés valident le master 1 en deux ans, pendant leur deuxième et leur troisième année. 
La première année est un tronc commun entre les masters portés par la faculté de médecine. 
La seconde année est spécifique à chaque master. 
Plus  d'informations sur le [livret double cursus médecine-sciences](https://medecine.univ-rennes1.fr/sites/medecine.univ-rennes1.fr/files/asset/document/livret_double_cursus.pdf). 
Pour bioinformatique :

- Bioinformatique et génomique 1 (3 ECTS)
- Programmation impérative en Python (6 ECTS)
- au choix :
    - Advanced statistics with R (3 ECTS)
    - Next Generation Sequencing (3 ECTS)
- Bases de données relationnelles ( 6 ECTS)
- Stage (12 ECTS)

## Vous êtes élève-ingénieur(e) en seconde année à l’ENSAI
Vous pouvez candidater auprès de l’[ENSAI](http://www.ensai.fr) pour bénéficier de l’OFPR au cours de votre troisième année afin d’obtenir à la fois votre diplôme d’ingénieur de l’ENSAI et le master de bioinformatique. 

### Filière « Sciences de la vie » → parcours [BIS (bioinformatique en santé)](bis.md)

- ENSAI : 12 ECTS
    - Essais cliniques (6 ECTS)
    - Statistiques pour données *omics (6 ECTS)
- Master bioinfo : 18 ECTS
    - Bioinformatique et génomique 1&2 (6 ECTS)
    - Biologie des systèmes : réseaux biologiques (6 ECTS)
    - Gene mapping and NGS analysis (6 ECTS)
- Stage : 30 ECTS

### Filière « Statistiques et ingénierie des données » → parcours [IBI (info. et bio. Intégrative)](ibi.md)

- ENSAI : 12 ECTS
    - Big data (6 ECTS)
    - Statistiques pour la fouille et le big data (6 ECTS)
- Master bioinfo : 18 ECTS
    - Bioinformatique et génomique 1&2 (6 ECTS)
    - Biologie des systèmes : réseaux biologiques (6 ECTS)
    - Algorithmique des séquences (6 ECTS)
- Stage : 30 ECTS

## Vous êtes étudiant(e) en reprise d'étude

Si vous êtes en reprise d'étude (ou si vous vous posez la question) :

- contactez le plus rapidement possible (il y a sans doute un dossier à remplir) avce le [Service de la formation continue](https://formation-continue.univ-rennes1.fr/)
- en parallèle, [contactez les responsables du master](mailto:master-bioinfo@univ-rennes1.fr)

