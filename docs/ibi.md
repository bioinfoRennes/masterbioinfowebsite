# Master 2 bioinformatique - parcours Informatique et Biologie Intégrative (IBI)

L'année de M1 est principalement consacrée à acquerir un socle équilibré de compétences fortes en informatique (~200h en M1) et en génomique-génétique (178h en M1). 
En informatique, l'accent est mis sur l'analyse et gestion de données massives, les systèmes informatiques et la programmation. 
Un socle de compétences en biologie et plus particulièrement en génomique et analyses de données issues des nouvelles technologies de séquençage sont également développées.

Dans le parcours **informatique et biologie intégrative**, la deuxième année de master comporte une spécialisation en algorithmique des séquences et en techniques d'optimisation qui permettent de mettre en pratique et en perspectives les compétences acquises au cours de la première année. 

Les cours ont lieu de septembre à décembre, et sont suivis par un stage de 6 mois.
Les stages à l'étranger sont encouragés !

## Compétences

![UE Master2 bioinformatique Rennes parcours IBI](img/schemaM2IBI-UE-2018.png)

![Récapitulatif Master2 bioinformatique Rennes parcours IBI](img/schemaM2IBI-recap-2018.png)

## Débouchés

Le parcours IBI débouche sur des postes d'ingénieur.e d'étude dans des équipes de recherche ou des plateformes en bioinformatique, dans l'industrie, ainsi que par une poursuite d'étude en doctorat.


## Stages types


