# Statistiques d'emploi

Pour un positionnement du master de bioinformatique de Rennes par rapport aux autres masters de France, consulter l'[état des lieux de la bioinformatique](https://www.sfbi.fr/sites/sfbi.fr/files/pictures/2017-10/fascicule_2018.pdf) élaboré par la SFBI.



## Cumul promotions 2017 à 2022

Il s'agit ici du cumul des informations recueillies chaque année en novembre sur le devenir des étudiant.e.s de la promotion sortante.

![Master bioinformatique Rennes : emploi des promotions 2017 à 2022](img/masterBioinfoRennes_cumul_emploi.png)


## Promotion 2022

36 étudiant.e.s de la promotion 2021 ont obtenu leur diplôme.
Nous les avons contacté.e.s début novembre 2022 pour leur demander leur situation professionnelle :

* 5 CDI
    * entreprises privées (5)
* 12 CDD
    * EFS, INRAe, INSERM, CHU, Inria, Genoscope, entreprises privées
    * Rennes, St Malo, Lyon, Paris, Nancy
* 8 thèses
    * Institut Curie, CRTI, NuMeCan, INRAe, COS 
    * Rennes, Toulouse, Paris, Nantes, Ljubljana, Suisse
* 2 poursuites d'études (médecine, vétérinaire)
* 0 reconversions professionnelles
* 4 recherches d'emploi
* 5 n'ont pas répondu

![Master bioinformatique Rennes : emploi de la promotion 2022](img/masterBioinfoRennes_promo2022_emploi.png)


## Promotion 2021

47 étudiant.e.s de la promotion 2021 ont obtenu leur diplôme.
Nous les avons contacté.e.s début novembre 2021 pour leur demander leur situation professionnelle :

* 4 CDI
    * CNRS, entreprises privées (3)
* 17 CDD
    * INRAe, INSERM, CNRS, CHU, CIRAD, Inst. Curie, entreprises privées
    * Rennes, St Malo, Lyon, Paris, Nantes, Montpellier, Lille, Genève, Québec
* 13 thèses
    * IRISA, LTSI, CHU, Institut Curie, IBSP, Karolinka Institutet
    * Rennes, Toulouse, Paris, Lille, Québec, Dublin, Stockholm
* 2 poursuites d'études (médecine, master informatique)
* 2 reconversions professionnelles
* 5 recherches d'emploi
* 7 n'ont pas répondu

![Master bioinformatique Rennes : emploi de la promotion 2021](img/masterBioinfoRennes_promo2021_emploi.png)



## Promotion 2020

40 étudiant.e.s de la promotion 2020 ont obtenu leur diplôme.
Nous les avons contacté.e.s début novembre 2020 pour leur demander leur situation professionnelle :

* 3 CDI
    * INRAe, entreprises privées (2)
    * Rennes, Paris, Canada
* 9 CDD
    * INRAe, Inria, structures hospitalières (2), IRISA, IRSET, entreprise privée
    * Rennes (5), Bordeaux, Lyon, Paris, Nantes
* 11 thèses
    * INRAe (2), IRISA (2), IRSET, IGDR, LTSI, CRTI Nantes, structure hospitalière, KU Leuven (BE)
    * Localisation : Rennes (5), Paris (2), Nantes, Tours, Leuven (BE)
* 3 poursuites d'études (pharmacie, master biologie, master informatique)
* 1 reconversion professionnelle
* 9 recherches d'emploi (effet crise covid ?)
* 4 n'ont pas répondu

![Master bioinformatique Rennes : emploi de la promotion 2020](img/masterBioinfoRennes_promo2020_emploi.png)



## Promotion 2019

30 étudiant.e.s de la promotion 2019 ont obtenu leur diplôme.
Nous les avons contacté.e.s début novembre 2019 pour leur demander leur situation professionnelle :

* 6 CDI
    * IFREMER, structure académique, entreprise privée
    * Localisation : Nice (2), Paris (2), Brest, Tour
* 4 CDD
    * INRA, Structure hospitalière, entreprise privée
    * Localisation : Rennes (3), Lyon
* 11 thèses
    * Inria (2), INSERM
    * Localisation : Rennes (3), Paris (2), Nantes (2), Bordeaux, Brest, Toulouse, Kiel (DE)
* 1 poursuite d'études
* 1 reconversion professionnelle
* 3 recherche d'emploi
* 4 n'ont pas répondu

![Master bioinformatique Rennes : emploi de la promotion 2019](img/masterBioinfoRennes_promo2019_emploi.png)


## Promotion 2018

30 étudiant.e.s de la promotion 2018 ont obtenu leur diplôme.
Nous les avons contacté.e.s début novembre 2018 pour leur demander leur situation professionnelle :

* 5 CDI
    * Employeur : Entreprise de Service du Numérique (4), INRA
    * Localisation : Paris (2), Milan, Nice, Rennes
* 9 CDD
    * Employeur : IRISA, INRA (2), Structures hospitalière (2), I2BC, Police scientifique
    * Localisation : Rennes (5), Bordeaux (2), région Parisienne (2)
* 6 thèses
    * 1 bourse "Marie Curie" (ex. Erasmus Mundus), 2 CIFRE, 2 CIRAD, 1 IRISA
    * Montpellier, Liège, Toulouse, Rennes
* 2 poursuites d'études (Master2 CCN, Rennes)
* 2 reconversions professionnelle
* 3 recherche d'emploi
* 2 n'ont pas répondu

![Master bioinformatique Rennes : emploi de la promotion 2018](img/masterBioinfoRennes_promo2018_emploi.png)

## Promotion 2017

La promotion 2017 était composée de 19 étudiant.e.s, ayant tous obtenu leur diplôme fin juin 2017.
Nous les avons contacté.e.s début novembre 2017 pour leur demander leur situation professionnelle :

* 1 CDI (en région Parisienne)
* 7 CDD
    * Employeur : INRA (2), INSERM, IRISA, INRIA, CNRS, SSII
    * Localisation : Rennes (3), Lille, Marseille, Toulouse, Turquie
* 6 thèses (+ 1 en attente)
    * 2 bourses INSERM-INRIA
    * 2 bourses CIFRE
* 1 poursuite d'étude (Master1 neurosciences, Paris)
* 1 recherche d'emploi
* 1 n'a pas répondu


![Master bioinformatique Rennes : emploi de la promotion 2017](img/masterBioinfoRennes_promo2017_emploi.png)
