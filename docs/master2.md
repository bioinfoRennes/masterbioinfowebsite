# Master 2 bioinformatique

Le master 2 est ouvert à tous les étudiant.e.s ayant validé le [master 1 de bioinformatique](master1.md) de Rennes.
Nous accueillons également volontiers les candidatures extérieures d'étudiant.e.s d'autres masters de bioinformatique, de cursus santé, d'écoles d'ingénieurs etc. (après validation de leur dossier).

## Organisation

Le master 2 comporte trois parcours :

* **[Analyse de données génomiques (ADG)](adg.md)** se spécialise sur l'utilisation de méthodes de pointe pour l'analyse de données *omiques, l'exploitation des bases de données de référence et l'utilisation d'infrastructures de calcul intensif. C'est la partie de la bioinformatique la plus proche de la biologie ;

* **[Bioinformatique en santé (BIS)](bis.md)** se spécialise sur les enjeux actuels de la génétique et de la génomique en santé : aide au diagnostic, génomique et cancérologie, génétique de l’hérédité simple et complexe, ingénierie génétique, ainsi que sur la conception de thérapeutiques nouvelles et personnalisées. C'est la partie de la bioinformatique centrée sur les données biomédicales ;

* **[Informatique et biologie intégrative (IBI)](ibi.md)** se spécialise sur le développement de méthodes novatrices d'analyse de données biologiques grâce à des compétences en algorithmique et en programmation. C'est la partie de la bioinformatique la plus proche de l'informatique.


## UE

La liste complète des UE et leurs descriptions sont détaillées dans [notre brochure](./BrochureBioinformatique2018.pdf) et sur le [site de l'offre de formation de l'université de Rennes 1](https://sve.univ-rennes1.fr/formations/master-1-bioinformatique).

![UE Master2 bioinformatique Rennes](img/schemaM2-2018.png)


