Source code for the Master's website at http://bioinfo-rennes.fr/

The site generator is [mkdocs](https://www.mkdocs.org/).
The files are in the ```doc``` directory. 

- To generate the static HTML files
    - run ```mkdocs build --clean```.
    - copy the content of the ```site``` directory on the server (use the private ```publishBioinfoWebsite.bash``` script)
- To serve the site locally, 
    - run ```mkdocs serve```
    - go to [http://127.0.0.1:8000/](http://127.0.0.1:8000/)
